package model;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class UserListTest {
    @Test
    public void add_user_in_list(){
        UserList people = new UserList();
        User userOne = new User("Salamy");
        User userTwo = new User("Robot");
        User userThree = new User("Rudolf");
        people.addUser(userOne);
        assertTrue(people.getPeopleList().contains(userOne));
        assertFalse(people.getPeopleList().contains(userTwo));
        assertFalse(people.getPeopleList().contains(userThree));
        people.addUser(userTwo);
        assertTrue(people.getPeopleList().contains(userOne));
        assertTrue(people.getPeopleList().contains(userTwo));
        assertFalse(people.getPeopleList().contains(userThree));
        people.addUser(userThree);
        assertTrue(people.getPeopleList().contains(userOne));
        assertTrue(people.getPeopleList().contains(userTwo));
        assertTrue(people.getPeopleList().contains(userThree));
    }

    @Test
    public void alphabetic_list_order(){
        UserList peopleList = new UserList();
        User userOne = new User("Salamy");
        User userTwo = new User("Robot");
        User userThree = new User("Arnorld");
        peopleList.addUser(userOne);
        peopleList.addUser(userTwo);
        peopleList.addUser(userThree);
        UserList alphabeticPeopleList = new UserList();
        alphabeticPeopleList.getPeopleList().addAll(peopleList.alphabeticOrder().getPeopleList());
        assertEquals(userThree.getName(), alphabeticPeopleList.getUser(0).getName());
        assertEquals(userTwo.getName(), alphabeticPeopleList.getUser(0));
        assertEquals(userOne.getName(), alphabeticPeopleList.getUser(0));
    }
}
