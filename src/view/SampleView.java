package view;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import model.User;
import model.UserList;

public class SampleView extends Application {
    UserList peopleList = new UserList();
    UserList shuffledPeopleList = new UserList();
    UserList alphabeticList = new UserList();
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        
        // partie gauche liste participants
        VBox left = new VBox();
        Button addBtn = new Button();
        Label title = new Label("Inscrits :");
        TextField input = new TextField();
        input.setMinSize(10, 30);
        addBtn.setText("+");

        Label peopleNameList = new Label("");
        left.getChildren().addAll(title, input, addBtn, peopleNameList);
        
        addBtn.setOnMouseClicked(e -> {
            // obtenir le nom donner dans la Vbox et le mettre dans la liste
            peopleList.addUser(new User(input.getText()));
            String nameList = "";
            for (int i = 0; i < peopleList.getPeopleList().size(); i++){
                nameList += peopleList.getUser(i).getName() + "\n";
            }
            peopleNameList.setText(nameList);
            input.setText("");
        });

        /////////////
        // partie droite shuffle et afficher

        VBox right = new VBox();
        Button btn = new Button();
        btn.setText("Nouveau tirage !");

        


        Label circularTitle = new Label("Affichage circulaire");
        TextArea circularContent = new TextArea();
        
        
        btn.setOnMouseClicked(e -> {
            circularContent.setText("");
            shuffledPeopleList.getPeopleList().clear();

            shuffledPeopleList.getPeopleList().addAll(peopleList.randomOrder().getPeopleList());

            circularContent.appendText(shuffledPeopleList.getUser(0).getName());
            for (int i = 1; i < shuffledPeopleList.getPeopleList().size(); i++){
                circularContent.appendText("\n -> " + shuffledPeopleList.getUser(i).getName());
            }

            //alphabeticList.getPeopleList().addAll(peopleList.alphabeticOrder().getPeopleList());

        });

        circularContent.setPrefSize(250,100);



        Label alphaTitle = new Label("Affichage alphabétique");
        TextArea alphaContent = new TextArea();
        alphaContent.setPrefSize(250,100);
        alphaContent.appendText("Example1 -> Example2");
        right.getChildren().addAll(btn, circularTitle, circularContent, alphaTitle, alphaContent);

        HBox root = new HBox(left, right);
        Scene scene = new Scene(root, 400, 400);
        primaryStage.setTitle("Tirage au sort des cadeaux");
        primaryStage.setScene(scene);
        primaryStage.show();
    }
}
