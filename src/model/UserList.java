package model;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class UserList {
    private List<User> people;

    public UserList(){
        this.people = new ArrayList<User>();
    }

    public UserList(List<User> Users){
        this.people = new ArrayList<User>();
        for (User user : Users)
        {
            this.people.add(user);
        }
        
    }

    public void addUser(User newUser){
        this.people.add(newUser);
    }

    /**
     * 
     * @return a random list with the original list
     */
    public UserList randomOrder(){
        Random rdn = new Random();

        List<User> save = new ArrayList<User>();
        List<User> newOrder = new ArrayList<User>();
        save.addAll(people);

        for (int i = 0; i < people.size(); i++){
            User tmp = save.get(rdn.nextInt(save.size())); 
            save.remove(tmp);
            newOrder.add(tmp);
        }

        return new UserList(newOrder);
    }

    public User getUser(int i){
        return (User) this.people.get(i);
    }

    public List<User> getPeopleList(){
        return this.people;
    }

    //Ne fonctionne pas score = 0 tout le temps
    public UserList alphabeticOrder() {
        int minScore = (int) this.getUser(0).getName().charAt(0);
        int indexMinScore = 0;
        int score;
        
        List<User> save = new ArrayList<User>();
        List<User> newOrder = new ArrayList<User>();
        save.addAll(people);
        for (int i = 0; i < people.size(); i++){
            for (int y = 0; y < save.size(); y++){
                score = (int) ((char) this.getUser(i).getName().charAt(0));
                if (score < minScore){
                    minScore = score;
                    indexMinScore = i;
                }
            }
            System.out.println(indexMinScore);
            User tmp = this.getPeopleList().get(indexMinScore); 
            save.remove(tmp);
            newOrder.add(tmp);
        }
        return new UserList(newOrder);
    }

}
